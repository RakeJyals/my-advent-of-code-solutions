input_list = []
while True:
    line = input()
    if line:
        input_list.append(line)
    else:
        break

twos = 0
threes = 0
for item in input_list:
	is_two = 0
	is_three = 0
	already_found = []
	for char in item:
		old_len = len(item)
		item = item.replace(char, "")
		if len(item) == old_len - 3:
			is_three = 1
		elif len(item) == old_len - 2:
			is_two = 1
	twos += is_two
	threes += is_three

print(twos * threes)
