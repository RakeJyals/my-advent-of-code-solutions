from listinput import inputCode
data_list = inputCode()
coordinate_list = set()
conflicting_coordinates = set()

for item in data_list:
	item = item.split("@")[1][1:].split(":")
	item[1] = item[1][1:].split("x")
	item[0] = item[0].split(",")
	coordinate_start = (int(item[0][0]), int(item[0][1]))
	for horiz_add in range(coordinate_start[0], coordinate_start[0] + int(item[1][0])):
		for vert_add in range(coordinate_start[1], coordinate_start[1] + int(item[1][1])):
			coordinate = (horiz_add, vert_add)
			if coordinate in coordinate_list:
				conflicting_coordinates.add(coordinate)
			else:
				coordinate_list.add(coordinate)

print(len(conflicting_coordinates))
