#Todo: figure out why example input works but puzzle input doesn't
from listinput import inputCode
data_list = inputCode()


def timeSort(string): #Algorithm for sorting list by time/date, puzzle input is out of order
	return int(string[1:16].replace("-", "").replace(":", "").replace(" ", "")) #Turns [YYYY-MM-DD HH:MM] into YYYYMMDDHHMM
data_list.sort(key=timeSort) #Semi-proven to sort perfectly


guard_list = dict()
"""Data format: each key is a guard id, corresponding items are arrays with a length of 61. (sleeping only occurs from 00:00 to 00:59)
[00:59] are the number of times the guard was asleep at that specific minute, and [60] is the sum of [00:59]
"""
for event in data_list: #Goes through all events
	if event[19] == "G": #New guard
		guard_id = int(event[26:len(event) - 13]) #Grabs the number following #
		if guard_id not in guard_list: #Initialize new guard
			guard_list[guard_id] = [] #Create a new entry in dictionary, handles unknown number of guards
			for i in range(0, 61): #0-59 for individual times, 60 for total, idk why range has to be 61
				guard_list[guard_id].append(0) #0 is default number of times asleep
		current_guard = guard_list[guard_id] #Used for wakeup
	elif event[19] == "f": #Falls asleep
		start_time = int(event[15:17])
	else: #Wakes up, might want to replace with elif
		#Code for adding new sleep times goes here
		end_time = int(event[15:17])
		for sleep_time in range(start_time, end_time):
			current_guard[sleep_time] += 1 #Adds 1 minute to all the minutes guard was asleep
			current_guard[60] += 1 #Increased the count of number of minutes asleep by 1

longest_sleep_time = 0 #Comparison variable
for guard in guard_list: #Determine which guard slept the most, iterator uses dictionary keys
	if guard_list[guard][60] > longest_sleep_time: #Compares total to largest found so far
		longest_sleep_guard = guard_list[guard][:60] #Array of all the minutes guard could be asleep
		longest_sleep_guard_id = guard #Dictionary key

		longest_sleep_time = guard_list[guard][60] #Comparison variable update


largest_minute_count = 0 #Comparison variable
test_var = 0 #REMOVE LATER
for minute in range(0, 60): #Determine at which minute the guard who slept the most is most likely to be asleep
#^Iterator uses a range of indexes (may not be called correct number of times?)
	test_var += 1 #REMOVE LATER
	print(str(minute) + ":" + str(longest_sleep_guard[minute])) #REMOVE LATER

	if longest_sleep_guard[minute] > largest_minute_count:
		largest_minute = minute

		largest_minute_count = longest_sleep_guard[minute]

#print("Standard guard length: " + str(len(guard_list[10])))
#10 comes from puzzle code id
print("Length of comparison array: " + str(len(longest_sleep_guard)))
print("Determine minute iteration count: "  + str(test_var))
print("Guard ID: " + str(longest_sleep_guard_id))
print("Slept Most At: 00:" + str(largest_minute))
print("Final Answer: " + str(longest_sleep_guard_id * largest_minute))

"""
Notes:
Found something wrong with my attempt to cut off the total from guard to longest_guard, should 
be fixed
Ranges also work weirdly, need to figure out where my logic doesn't line up with reality
Previous problems should be fixed, still getting same incorrect result with puzzle input
Sample input still works
"""
