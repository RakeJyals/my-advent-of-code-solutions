input_list = []
while True:
    line = input()
    if line:
        input_list.append(line)
    else:
        break

most_common_char_count = 0
print("Initial: " + str(input_list))
print(len(input_list))
for input_num in range(len(input_list)):
	input1 = input_list[0]
	input_list.pop(0)
	for input2 in input_list:
		print("Comparing " + input1 + " and " + input2)
		common_char_count = 0
		common_chars = ""
		for spot in range(len(input1)):
			if input1[spot] == input2[spot]:
				common_char_count += 1
				common_chars += input1[spot]
		print("Common chars: " + common_chars)
		if common_char_count > most_common_char_count:
			print("New best match")
			most_common_char_count = common_char_count
			most_common_chars = common_chars

print("Left over: " + str(input_list))
print("Final: " + most_common_chars)
