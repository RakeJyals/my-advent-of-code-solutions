from listinput import inputCode
data_list = inputCode()
coordinate_list_raw = set()
conflicting_coordinates = set()
coordinate_list_structured = dict()

for item in data_list:
	id = item.split(" ")[0][1:]
	item = item.split("@")[1][1:].split(":")
	item[1] = item[1][1:].split("x")
	item[0] = item[0].split(",")
	coordinates = []
	coordinate_start = (int(item[0][0]), int(item[0][1]))
	for horiz_add in range(coordinate_start[0], coordinate_start[0] + int(item[1][0])):
		for vert_add in range(coordinate_start[1], coordinate_start[1] + int(item[1][1])):
			coordinate = (horiz_add, vert_add)
			coordinates.append(coordinate)
			if coordinate in coordinate_list_raw:
				conflicting_coordinates.add(coordinate)
			else:
				coordinate_list_raw.add(coordinate)

	coordinate_list_structured[id] = coordinates


for id in coordinate_list_structured:
	conflicting = False
	check_list = coordinate_list_structured[id]
	for coordinate in check_list:
		if coordinate in conflicting_coordinates:
			conflicting = True
	if not conflicting:
		print(id)
